package account

import (
	"app/db"
	"crypto/md5"
	"fmt"
	"strings"
	"time"
)

// SaltThePass возвращает посоленый пароль
func SaltThePass(pass, salt string) string { return hash(pass + salt) }

// RandomSalt возвращает рандомную соль, в случае высокой нагрузки требуется переписать с добавление рандома и т.д.
func RandomSalt(k string) string { return hash(time.Now().String() + k) }

// hash функция для хеширования
func hash(s string) string { return fmt.Sprintf("%x", md5.Sum([]byte(s))) }

// CreateAccount создает аккаунт
func CreateAccount(login, password string) (int, error) {
	salt := RandomSalt(login)
	password = SaltThePass(password, salt)
	id, err := db.InsertAccount(strings.ToLower(login), login, password, salt)
	if err != nil && strings.Index(err.Error(), "UNIQUE constraint failed: users.login") != -1 {
		err = fmt.Errorf("Логин уже занят")
	}
	return id, err
}

// Login авторизирует пользователя
func Login(login, password string) (string, error) {
	acc, err := db.SelectAccountByLogin(strings.ToLower(login))
	if err != nil {
		return "", fmt.Errorf("Логина нет в базе")
	}
	if SaltThePass(password, acc.Salt) != acc.Password {
		return "", fmt.Errorf("Не корректный пароль")
	}
	token := RandomSalt(acc.Login)
	if db.InsertSession(token, acc.ID) != nil {
		return "", fmt.Errorf("Ошибка создания сессии")
	}
	return token, nil
}
