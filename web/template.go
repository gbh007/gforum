package web

import (
	"html/template"
	"log"
)

var tmpl = template.New("")

func init() {
	var err error
	tmpl, err = tmpl.ParseGlob("static/*.html")
	if err != nil {
		log.Panicln(err)
	}
}
