package web

import (
	"app/config"
	"app/db"
	"fmt"
	"net/http"
	"os"
	"path"
	"strconv"
)

// GetFile возвращает файл по имени
func GetFile(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil {
		SetError(r, ErrParseData)
		return
	}
	fileInfo, err := db.SelectFileByID(id)
	if err != nil {
		SetError(r, ErrNotFound)
		return
	}
	f, err := os.Open(path.Join(config.DefaultFilePath, fmt.Sprint(id)))
	if err != nil {
		SetError(r, ErrNotFound)
		return
	}
	SetResponse(r, f)
	if r.URL.Query().Get("load") == "true" {
		SetFilename(r, fileInfo.Name)
	}
	SetFileType(r, fileInfo.Mime)
}
