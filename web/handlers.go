package web

import (
	"app/db"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

// FileWriteHandler хандлер для ответа в виде файла
func FileWriteHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// возвращаем только заголовок если это OPTIONS
		if r.Method == http.MethodOptions {
			return
		}
		// обрабатываем следующий хандлер
		if next != nil {
			func() {
				defer func() {
					if recover() != nil {
						SetError(r, fmt.Errorf("Внутреняя ошибка сервера"))
					}
				}()
				next.ServeHTTP(w, r)
			}()
		}
		// обрабатываем ошибку если есть
		if err := r.Context().Err(); err != nil {
			w.Header().Set("Content-Type", "text/plain; charset=utf-8")
			switch err {
			case ErrForbidden:
				w.WriteHeader(http.StatusForbidden)
			case ErrNotFound:
				w.WriteHeader(http.StatusNotFound)
			case ErrNotAuth:
				w.WriteHeader(http.StatusUnauthorized)
			case ErrParseData:
				w.WriteHeader(http.StatusBadRequest)
			case ErrToLargeData:
				w.WriteHeader(http.StatusRequestEntityTooLarge)
			default:
				w.WriteHeader(http.StatusInternalServerError)
			}
			if _, err := io.WriteString(w, err.Error()); err != nil {
				log.Println(err)
			}
			return
		}
		// установка типа вложения
		filename := r.Context().Value(ResponseFilenameKey)
		if filename != nil {
			w.Header().Set("Content-Disposition", `attachment; filename="`+fmt.Sprint(filename)+`"`)
		}
		// установка типа файла
		filetype := r.Context().Value(ResponseFileTypeKey)
		if filetype != nil {
			w.Header().Set("Content-Type", fmt.Sprint(filetype))
		}
		w.WriteHeader(http.StatusOK)
		// получение данных
		data := r.Context().Value(ResponseDataKey)
		if data == nil {
			return
		}
		// определение типа данных
		switch f := data.(type) {
		case io.ReadCloser:
			defer f.Close()
			if _, err := io.Copy(w, f); err != nil {
				log.Println(err)
			}
		case io.Reader:
			if _, err := io.Copy(w, f); err != nil {
				log.Println(err)
			}
		}
	})
}

// ResponseData данные для использования шаблонами
type ResponseData struct {
	Account  db.Account    // информация об аккаунте
	Data     interface{}   // данные для шаблона
	Time     time.Time     // время на сервере
	Duration time.Duration // продолжительность выполнения на сервере
}

// TemplateHandler хандлер для ответа в виде json
func TemplateHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestStart := time.Now()
		w.Header().Set("Content-Type", "text/html")
		if r.Method == http.MethodOptions {
			return
		}
		if next != nil {
			func() {
				defer func() {
					if recover() != nil {
						SetError(r, fmt.Errorf("Внутреняя ошибка сервера"))
					}
				}()
				next.ServeHTTP(w, r)
			}()
		}
		acc, _ := GetAccount(r)
		if err := r.Context().Err(); err != nil {
			switch err {
			case ErrForbidden:
				w.WriteHeader(http.StatusForbidden)
			case ErrNotFound:
				w.WriteHeader(http.StatusNotFound)
			case ErrNotAuth:
				w.WriteHeader(http.StatusUnauthorized)
			case ErrParseData:
				w.WriteHeader(http.StatusBadRequest)
			case ErrToLargeData:
				w.WriteHeader(http.StatusRequestEntityTooLarge)
			default:
				w.WriteHeader(http.StatusInternalServerError)
			}
			if err := tmpl.ExecuteTemplate(w, "error", ResponseData{
				Account:  acc,
				Data:     err,
				Time:     requestStart,
				Duration: time.Now().Sub(requestStart),
			}); err != nil {
				log.Println(err)
			}
			return
		}
		if tmp, ok := r.Context().Value(ResponseRedirectKey).(string); ok {
			http.Redirect(w, r, tmp, http.StatusTemporaryRedirect)
			return
		}
		templateName, ok := r.Context().Value(ResponseTemplateNameKey).(string)
		if !ok {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprint(w, "не корректный тип шаблона")
			return
		}
		w.WriteHeader(http.StatusOK)
		data := ResponseData{
			Account:  acc,
			Data:     r.Context().Value(ResponseDataKey),
			Time:     requestStart,
			Duration: time.Now().Sub(requestStart),
		}
		if err := tmpl.ExecuteTemplate(w, templateName, data); err != nil {
			log.Println(err)
		}
	})
}

// SessionHandler обработчик сессии пользователя
func SessionHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u, err := CheckSession(r)
		if err == nil {
			// устанавливаем в контекст данные пользователя
			SetData(r, ResponseUserKey, u)
		}
		next.ServeHTTP(w, r)
	})
}

// AuthRequireHandler проверяет авторизован ли пользователь (отсекает 401 ошибкой не авторизованных)
func AuthRequireHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, login := GetAccount(r)
		if !login {
			SetError(r, ErrNotAuth)
			return
		}
		next.ServeHTTP(w, r)
	})
}

// AddHandler добавляет в мукс обработчик JSON
func AddHandler(mux *http.ServeMux, uri string, auth bool, next http.Handler) {
	if auth {
		mux.Handle(uri, TemplateHandler(SessionHandler(AuthRequireHandler(next))))
	} else {
		mux.Handle(uri, TemplateHandler(SessionHandler(next)))
	}
}

// AddFileHandler добавляет в мукс обработчик файлов
func AddFileHandler(mux *http.ServeMux, uri string, next http.Handler) {
	mux.Handle(uri, FileWriteHandler(SessionHandler(AuthRequireHandler(next))))
}
