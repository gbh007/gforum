package web

import (
	"app/db"
	"fmt"
	"net"
	"net/http"
	"strings"
)

// SessionTokenName имя печеньки сессии
const SessionTokenName = "forum-session"

// перменные ошибок
var (
	ErrNotAuth        = fmt.Errorf("Необходима авторизация")
	ErrForbidden      = fmt.Errorf("Нет прав доступа")
	ErrNotFound       = fmt.Errorf("Данные не найдены")
	ErrParseData      = fmt.Errorf("Некорректный формат данных")
	ErrDeleteData     = fmt.Errorf("Невозможно удалить данные")
	ErrCreateData     = fmt.Errorf("Невозможно создать данные")
	ErrAppendData     = fmt.Errorf("Невозможно добавить данные")
	ErrUpdateData     = fmt.Errorf("Невозможно изменить данные")
	ErrProcessingData = fmt.Errorf("Невозможно обработать данные")
	ErrToLargeData    = fmt.Errorf("Слишком большой объем данных")
)

// GetSessionToken получает токен из печеньки
func GetSessionToken(r *http.Request) (token string, err error) {
	cookie, err := r.Cookie(SessionTokenName)
	if err != nil {
		return
	}
	token = cookie.Value
	return
}

// CheckSession проверяет сессию
func CheckSession(r *http.Request) (u db.Account, err error) {
	token, err := GetSessionToken(r)
	if err != nil {
		return
	}
	u, err = db.SelectAccountBySession(token)
	return
}

// GetIP возвращает реальный ip пользователя
func GetIP(r *http.Request) string {
	if ip := r.Header.Get("X-REAL-IP"); ip != "" {
		return ip
	}
	if ips := r.Header.Get("X-FORWARDED-FOR"); ips != "" {
		for _, ip := range strings.Split(ips, ",") {
			return ip
		}
	}
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return ""
	}
	return ip
}

// GetAccount возвращает данные о пользователе
// используется только после прохождения запросом SessionHandler (вносит данные в запрос)
func GetAccount(r *http.Request) (db.Account, bool) {
	u, ok := r.Context().Value(ResponseUserKey).(db.Account)
	return u, ok && u.ID > 0
}

// GetAccountInfo возвращает информацию об аккаунте с которого выполняется запрос
// формат выходной строки [USER_LOGIN/IP] или [/IP]
func GetAccountInfo(r *http.Request) string {
	info := "["
	u, ok := GetAccount(r)
	if ok {
		info += u.Login
	}
	info += "/" + GetIP(r) + "]"
	return info
}
