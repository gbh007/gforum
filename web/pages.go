package web

import (
	"app/account"
	"app/db"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// ItemsOnPage количество предметов (форумов/сообщений) на странице
const ItemsOnPage = 20

// MaxFileSize максимальный размер файла
const MaxFileSize = 10 * 1024 * 1024

// GetMainPage возвращает главную страницу
func GetMainPage(w http.ResponseWriter, r *http.Request) {
	page, _ := strconv.Atoi(r.URL.Query().Get("page"))
	if page < 1 {
		page = 1
	}
	count, _ := db.SelectForumCount()
	if count%ItemsOnPage > 0 {
		count += ItemsOnPage
	}
	count /= ItemsOnPage
	pages := []int{}
	if count > 1 {
		for i := 1; i <= count; i++ {
			pages = append(pages, i)
		}
	}
	SetResponse(r, map[string]interface{}{
		"Forums":  db.SelectForums((page-1)*ItemsOnPage, ItemsOnPage),
		"Pages":   pages,
		"Page":    page,
		"PageURL": "/?page=",
	})
	SetTemplateName(r, "main")
}

// GetRegistrationPage возвращает страницу регистрации
func GetRegistrationPage(w http.ResponseWriter, r *http.Request) {
	SetTemplateName(r, "registration")
}

// NewAccount регистрирует пользователя
func NewAccount(w http.ResponseWriter, r *http.Request) {
	login := r.FormValue("login")
	password := r.FormValue("password")
	if login == "" || password == "" {
		SetError(r, fmt.Errorf("Не заполнен логин или пароль"))
		return
	}
	id, err := account.CreateAccount(login, password)
	if err != nil {
		SetError(r, err)
		return
	}
	SetTemplateName(r, "success")
	SetResponse(r, id)
}

// Login авторизирует пользователя
func Login(w http.ResponseWriter, r *http.Request) {
	login := r.FormValue("login")
	password := r.FormValue("password")
	if login == "" || password == "" {
		SetError(r, fmt.Errorf("Не заполнен логин или пароль"))
		return
	}
	token, err := account.Login(login, password)
	if err != nil {
		SetError(r, err)
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name:     SessionTokenName,
		Value:    token,
		Path:     "/",
		HttpOnly: true,
	})
	SetRedirect(r, "/")
	// SetTemplateName(r, "success")
}

// Logout деавторизирует пользователя
func Logout(w http.ResponseWriter, r *http.Request) {
	token, err := GetSessionToken(r)
	if err != nil {
		SetError(r, err)
		return
	}
	db.DeleteSession(token)
	http.SetCookie(w, &http.Cookie{
		Name:     SessionTokenName,
		Value:    "",
		Path:     "/",
		HttpOnly: true,
		Expires:  time.Now(),
	})
	SetTemplateName(r, "success")
	SetData(r, ResponseUserKey, nil) // обнуляем пользователя для последующих операций
}

// GetLoginPage возвращает страницу авторизации
func GetLoginPage(w http.ResponseWriter, r *http.Request) {
	SetTemplateName(r, "login")
}

// GetDebugPage возвращает страницу дебага
func GetDebugPage(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		SetError(r, err)
		return
	}
	SetTemplateName(r, "debug")
	SetResponse(r, r.Form)
}

// NewForum создает новый форум
func NewForum(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	if name == "" {
		SetError(r, fmt.Errorf("Не указано название форума"))
		return
	}
	acc, _ := GetAccount(r)
	id, err := db.InsertForum(name, acc.ID)
	if err != nil {
		if strings.Index(err.Error(), "UNIQUE constraint failed") != -1 {
			SetError(r, fmt.Errorf("Название форума уже занято"))
		} else {
			SetError(r, err)
		}
		return
	}

	SetRedirect(r, fmt.Sprintf("/forum/%d?page=1", id))
	// SetTemplateName(r, "success")
	// SetResponse(r, id)
}

// GetForumPage возвращает страницу форума
func GetForumPage(w http.ResponseWriter, r *http.Request) {
	page, _ := strconv.Atoi(r.URL.Query().Get("page"))
	if page < 1 {
		page = 1
	}
	tmp := strings.Split(strings.Trim(r.URL.Path, "/"), "/")
	if len(tmp) != 2 || tmp[0] != "forum" {
		SetError(r, fmt.Errorf("Ошибка адресации"))
		return
	}
	forumID, err := strconv.Atoi(tmp[1])
	if err != nil {
		SetError(r, err)
		return
	}
	forum, err := db.SelectForumByID(forumID)
	if err != nil {
		SetError(r, fmt.Errorf("Нет такого форума"))
		return
	}
	count, _ := db.SelectForumMessageCount(forumID)
	if count%ItemsOnPage > 0 {
		count += ItemsOnPage
	}
	count /= ItemsOnPage
	pages := []int{}
	if count > 1 {
		for i := 1; i <= count; i++ {
			pages = append(pages, i)
		}
	}
	SetResponse(r, map[string]interface{}{
		"ForumID":  forumID,
		"Forum":    forum,
		"Pages":    pages,
		"Page":     page,
		"PageURL":  fmt.Sprintf("/forum/%d?page=", forumID),
		"LastPage": page == count || count == 0,
		"Messages": db.SelectForumMessages(forumID, (page-1)*ItemsOnPage, ItemsOnPage),
	})
	SetTemplateName(r, "forum")
}
