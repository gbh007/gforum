package web

import (
	"app/config"
	"app/db"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path"
	"strconv"
	"time"
)

// NewMessage создает новое сообщение на форум
func NewMessage(w http.ResponseWriter, r *http.Request) {
	// ограничивается максимальным размером файла
	err := r.ParseMultipartForm(MaxFileSize)
	if err == multipart.ErrMessageTooLarge {
		SetError(r, ErrToLargeData)
		return
	}
	if err != nil {
		SetError(r, err)
		return
	}

	message := r.FormValue("message")
	if message == "" {
		SetError(r, fmt.Errorf("Пустое сообщение"))
		return
	}
	acc, _ := GetAccount(r)
	forumID, err := strconv.Atoi(r.FormValue("forum-id"))
	if err != nil {
		SetError(r, fmt.Errorf("Некорректный адрес форума"))
		return
	}
	page, _ := strconv.Atoi(r.FormValue("page"))
	if page < 1 {
		page = 1
	}
	id, err := db.InsertMessage(message, forumID, acc.ID)
	if err != nil {
		SetError(r, err)
		return
	}
	for _, fHead := range r.MultipartForm.File["file"] {
		fid, err := db.InsertFile(db.File{
			Name:       fHead.Filename,
			Mime:       fHead.Header.Get("Content-Type"),
			Uploaded:       time.Now(),
			UploaderID: acc.ID,
			Size:       fHead.Size,
			MessageID:  id,
		})
		if err != nil {
			SetError(r, err)
			return
		}
		fIn, err := fHead.Open()
		if err != nil {
			SetError(r, err)
			return
		}
		defer fIn.Close()
		fOut, err := os.Create(path.Join(config.DefaultFilePath, fmt.Sprint(fid)))
		if err != nil {
			SetError(r, err)
			return
		}
		defer fOut.Close()
		_, err = io.Copy(fOut, fIn)
		if err != nil {
			SetError(r, err)
			return
		}
	}

	SetRedirect(r, fmt.Sprintf("/forum/%d?page=%d", forumID, page))
}
