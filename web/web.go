package web

import (
	"log"
	"net/http"
)

// Run запускает веб сервер
func Run(addr string) <-chan struct{} {
	mux := http.NewServeMux()

	AddHandler(mux, "/", false, http.HandlerFunc(GetMainPage))
	AddHandler(mux, "/page/registration", false, http.HandlerFunc(GetRegistrationPage))
	AddHandler(mux, "/page/login", false, http.HandlerFunc(GetLoginPage))
	AddHandler(mux, "/account/new", false, http.HandlerFunc(NewAccount))
	AddHandler(mux, "/account/login", false, http.HandlerFunc(Login))
	AddHandler(mux, "/account/logout", false, http.HandlerFunc(Logout))
	AddHandler(mux, "/forum/new", true, http.HandlerFunc(NewForum))
	AddHandler(mux, "/forum/new-message", true, http.HandlerFunc(NewMessage))
	AddHandler(mux, "/forum/", false, http.HandlerFunc(GetForumPage))
	AddFileHandler(mux, "/file", http.HandlerFunc(GetFile))

	// отладка
	AddHandler(mux, "/debug", false, http.HandlerFunc(GetDebugPage))

	// mux.HandleFunc("/drt", func(rw http.ResponseWriter, r *http.Request) {
	// 	var err error
	// 	tmpl, err = template.New("").ParseGlob("static/*.html")
	// 	if err != nil {
	// 		log.Println(err)
	// 	}
	// })

	// создание объекта сервера
	server := &http.Server{
		Addr:    addr,
		Handler: mux,
	}
	done := make(chan struct{})
	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Println(err)
		}
		close(done)
	}()
	return done
}
