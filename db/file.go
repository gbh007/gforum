package db

import (
	"database/sql"
	"log"
	"strings"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

// File структура для файла
type File struct {
	ID         int
	Name       string
	Mime       string // MIME type (тип файла)
	Uploaded   time.Time
	UploaderID int
	Size       int64
	AvatarID   int
	MessageID  int
	// дополнительно вычисляемые поля
	IsImage bool
}

// SelectFileByID выбирает из базы файл по ID
func SelectFileByID(id int) (File, error) {
	row := _db.QueryRow(`SELECT id, name, mime, uploaded, uploader_id, size, avatar_id, message_id FROM files WHERE id = ?`, id)
	f := File{}
	var (
		name, mime          sql.NullString
		avatarID, messageID sql.NullInt64
	)
	err := row.Scan(
		&f.ID,
		&name,
		&mime,
		&f.Uploaded,
		&f.UploaderID,
		&f.Size,
		&avatarID,
		&messageID,
	)
	if err != nil {
		log.Println(err)
	} else {
		f.Name = name.String
		f.Mime = mime.String
		f.AvatarID = int(avatarID.Int64)
		f.MessageID = int(messageID.Int64)
	}
	return f, err
}

// SelectFileByAvatarID выбирает из базы файл по ID пользователя
func SelectFileByAvatarID(id int) (File, error) {
	row := _db.QueryRow(`SELECT id, name, mime, uploaded, uploader_id, size, avatar_id, message_id FROM files WHERE avatar_id = ?`, id)
	f := File{}
	var (
		name, mime          sql.NullString
		avatarID, messageID sql.NullInt64
	)
	err := row.Scan(
		&f.ID,
		&name,
		&mime,
		&f.Uploaded,
		&f.UploaderID,
		&f.Size,
		&avatarID,
		&messageID,
	)
	if err != nil {
		log.Println(err)
	} else {
		f.Name = name.String
		f.Mime = mime.String
		f.AvatarID = int(avatarID.Int64)
		f.MessageID = int(messageID.Int64)
	}
	return f, err
}

// InsertFile добавляет новый файл в базу
func InsertFile(f File) (int, error) {
	result, err := _db.Exec(
		`INSERT INTO files(name, mime, uploaded, uploader_id, size, avatar_id, message_id) VALUES(?, ?, ?, ?, ?, ?, ?)`,
		sql.NullString{String: f.Name, Valid: f.Name != ""},
		sql.NullString{String: f.Mime, Valid: f.Mime != ""},
		f.Uploaded,
		f.UploaderID,
		f.Size,
		sql.NullInt64{Int64: int64(f.AvatarID), Valid: f.AvatarID > 0},
		sql.NullInt64{Int64: int64(f.MessageID), Valid: f.MessageID > 0},
	)
	if err != nil {
		log.Println(err)
		return -1, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		log.Println(err)
		return -1, err
	}
	return int(id), nil
}

// SelectFilesByMessageID выбирает из базы список файлов прикрипленных к сообщению
func SelectFilesByMessageID(id int) []File {
	res := []File{}
	rows, err := _db.Query(`SELECT id, name, mime, uploaded, uploader_id, size, avatar_id, message_id FROM files WHERE message_id = ?`, id)
	if err != nil {
		log.Println(err)
		return res
	}
	for rows.Next() {
		f := File{}
		var (
			name, mime          sql.NullString
			avatarID, messageID sql.NullInt64
		)
		err = rows.Scan(
			&f.ID,
			&name,
			&mime,
			&f.Uploaded,
			&f.UploaderID,
			&f.Size,
			&avatarID,
			&messageID,
		)
		if err != nil {
			log.Println(err)
		} else {
			f.Name = name.String
			f.Mime = mime.String
			f.AvatarID = int(avatarID.Int64)
			f.MessageID = int(messageID.Int64)
			f.IsImage = strings.Index(f.Mime, "image") == 0
			res = append(res, f)
		}
	}
	return res
}
