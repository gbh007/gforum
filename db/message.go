package db

import (
	"log"
	"time"
)

type Message struct {
	ID       int       // id сообщения
	Message  string    // текст сообщения
	ForumID  int       // id форума
	SenderID int       // id отправителя
	Sended   time.Time // время отправки сообщения
	// вычисляются отдельно
	Sender      Account // отправитель
	Attachments []File  // вложения
}

// SelectForumMessages выбирает из базы список сообщений на форуме по ограничениям
func SelectForumMessages(forumID, offset, limit int) []Message {
	res := []Message{}
	rows, err := _db.Query(`SELECT id, message, forum_id, sender_id, sended
FROM messages WHERE forum_id = ? ORDER BY sended LIMIT ?, ?`, forumID, offset, limit)
	if err != nil {
		log.Println(err)
		return res
	}
	for rows.Next() {
		m := Message{}
		err = rows.Scan(
			&m.ID,
			&m.Message,
			&m.ForumID,
			&m.SenderID,
			&m.Sended,
		)
		if err != nil {
			log.Println(err)
		} else {
			// игнорируем проверку ошибок
			m.Sender, _ = SelectAccountByID(m.SenderID)
			m.Attachments = SelectFilesByMessageID(m.ID)
			res = append(res, m)
		}
	}
	return res
}

// InsertMessage добавляет новое сообщение на форум в базу
func InsertMessage(message string, forumID, senderID int) (int, error) {
	result, err := _db.Exec(
		`INSERT INTO messages(message, forum_id, sender_id, sended) VALUES(?, ?, ?, ?)`,
		message, forumID, senderID, time.Now(),
	)
	if err != nil {
		log.Println(err)
		return -1, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		log.Println(err)
		return -1, err
	}
	return int(id), nil
}
