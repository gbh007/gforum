package db

import (
	"log"
)

// InsertForum добавляет новый форум в базу
func InsertForum(name string, creatorID int) (int, error) {
	result, err := _db.Exec(
		`INSERT INTO forums(name, creator_id) VALUES(?, ?)`,
		name, creatorID,
	)
	if err != nil {
		log.Println(err)
		return -1, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		log.Println(err)
		return -1, err
	}
	return int(id), nil
}

type Forum struct {
	ID        int    // ид форума
	Name      string // название форума
	CreatorID int    // ид его создателя
	// вычисляются отдельно
	Creator      Account // данные создателя
	MessageCount int     // количество сообщений
}

// SelectForumMessageCount получает количество сообщений на форуме (потоке) из базы
func SelectForumMessageCount(id int) (int, error) {
	row := _db.QueryRow(`SELECT COUNT(id) FROM messages WHERE forum_id = ?`, id)
	var count int
	err := row.Scan(&count)
	if err != nil {
		log.Println(err)
	}
	return count, err
}

// SelectForums выбирает из базы список форумов по ограничениям
func SelectForums(offset, limit int) []Forum {
	res := []Forum{}
	rows, err := _db.Query(`SELECT f.id, f.name, f.creator_id
FROM forums f ORDER BY f.id DESC LIMIT ?, ?`, offset, limit)
	if err != nil {
		log.Println(err)
		return res
	}
	for rows.Next() {
		f := Forum{}
		err = rows.Scan(
			&f.ID,
			&f.Name,
			&f.CreatorID,
		)
		if err != nil {
			log.Println(err)
		} else {
			// игнорируем проверку ошибок
			f.Creator, _ = SelectAccountByID(f.CreatorID)
			f.MessageCount, _ = SelectForumMessageCount(f.ID)
			res = append(res, f)
		}
	}
	return res
}

// SelectForumByID выбирает из базы форум по ID
func SelectForumByID(id int) (Forum, error) {
	row := _db.QueryRow(`SELECT f.id, f.name, f.creator_id FROM forums f WHERE f.id = ?`, id)
	f := Forum{}
	err := row.Scan(
		&f.ID,
		&f.Name,
		&f.CreatorID,
	)
	if err != nil {
		log.Println(err)
	} else {
		// игнорируем проверку ошибок
		f.Creator, _ = SelectAccountByID(f.CreatorID)
		f.MessageCount, _ = SelectForumMessageCount(f.ID)
	}
	return f, err
}

// SelectForumCount получает количество форумов из базы
func SelectForumCount() (int, error) {
	row := _db.QueryRow(`SELECT COUNT(id) FROM forums`)
	var count int
	err := row.Scan(&count)
	if err != nil {
		log.Println(err)
	}
	return count, err
}
