package db

import (
	"log"
	"time"
)

type Account struct {
	ID           int
	Login        string
	LoginRaw     string
	Password     string
	Salt         string
	Registration time.Time
}

func (a *Account) scan(row interface {
	Scan(dest ...interface{}) error
}) error {
	return row.Scan(
		&a.ID,
		&a.Login,
		&a.LoginRaw,
		&a.Password,
		&a.Salt,
		&a.Registration,
	)
}

// InsertAccount заносит данные аккаунта в базу
func InsertAccount(login, loginRaw, password, salt string) (int, error) {
	result, err := _db.Exec(
		`INSERT INTO users(login, login_raw, password, salt, registration) VALUES(?, ?, ?, ?, ?)`,
		login, loginRaw, password, salt, time.Now(),
	)
	if err != nil {
		log.Println(err)
		return -1, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		log.Println(err)
		return -1, err
	}
	return int(id), nil
}

// SelectAccountByLogin выбирает из базы аккаунт по логину
func SelectAccountByLogin(login string) (Account, error) {
	row := _db.QueryRow(`SELECT u.id, u.login, u.login_raw, u.password, u.salt, u.registration
FROM users u
WHERE u.login = ?`, login)
	a := Account{}
	err := a.scan(row)
	if err != nil {
		log.Println(err)
	}
	return a, err
}

// SelectAccountByID выбирает из базы аккаунт по ID
func SelectAccountByID(id int) (Account, error) {
	row := _db.QueryRow(`SELECT u.id, u.login, u.login_raw, u.password, u.salt, u.registration
FROM users u
WHERE u.id = ?`, id)
	a := Account{}
	err := a.scan(row)
	if err != nil {
		log.Println(err)
	}
	return a, err
}

// SelectAccountBySession выбирает из базы аккаунт по ключу сессии
func SelectAccountBySession(token string) (Account, error) {
	row := _db.QueryRow(`SELECT u.id, u.login, u.login_raw, u.password, u.salt, u.registration
FROM users u INNER JOIN sessions s ON u.id = s.user_id
WHERE s.token = ?`, token)
	a := Account{}
	err := a.scan(row)
	if err != nil {
		log.Println(err)
	}
	return a, err
}

// InsertSession заносит данные сессии в базу
func InsertSession(token string, user_id int) error {
	_, err := _db.Exec(
		`INSERT INTO sessions(token, user_id, creation) VALUES(?, ?, ?)`,
		token, user_id, time.Now(),
	)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

// DeleteSession удаляет сессию из базы
func DeleteSession(token string) error {
	_, err := _db.Exec(`DELETE FROM sessions WHERE token = ?`, token)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
