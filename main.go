package main

import (
	"app/config"
	"app/db"
	"app/web"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	webPort := flag.Int("p", 8080, "порт веб сервера")
	flag.Parse()
	lf, err := os.Create("log.txt")
	if err != nil {
		log.Println(err)
		return
	}
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.SetOutput(io.MultiWriter(os.Stderr, lf))

	// создание папки для хранения файлов
	err = os.MkdirAll(config.DefaultFilePath, 0777)
	if err != nil && !os.IsExist(err) {
		log.Println(err)
		return
	}

	db.Connect()

	done := web.Run(fmt.Sprintf(":%d", *webPort))
	<-done
}
